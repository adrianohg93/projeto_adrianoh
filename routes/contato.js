var express = require('express');
var Contato = require('../models/contato');
var router = express.Router();

router.post('/', (req, res)=>{
    Contato.insert(req, res);
});
router.get('/', (req, res)=>{
    Contato.getAll(res);
});
router.get('/:id', (req, res)=>{
    Contato.getById(req.params.id, res);
});
router.delete('/:id', (req, res)=>{
    Contato.delete(req.params.id, res);
});
router.put('/:id', (req, res)=>{
    Contato.update(req, res);
});
module.exports = router;