var Mongoose = require('./index').mongoose;


var contatoSchema = Mongoose.Schema({
    nome: {type: String, required: true, unique: true},
    email: {type: String, required: true, unique: true},
	telefone: {type: String, required: true, unique: true},
});

var Contato = Mongoose.model('Contato', contatoSchema);

const functions = {
    insert: (req, res)=>{
        var contato = new Contato(req.body);
        contato.save((error, result)=>{
            if(error) res.json(error);
            res.json(result);
        });
    },
    getAll: (res)=>{
        Contato.find({}, (error, result)=>{
            if(error) res.json(error);
            res.json(result);
        });
    },
    getById: (id, res)=>{
        Contato.findById(id, (error, inst)=>{
            if(error) res.json(error);
            res.json(inst);
        });
    },
    update: (req, res)=>{
        Contato.findByIdAndUpdate(req.params.id, req.body, (error, result)=>{
            if(error) res.json(error);
            res.json(result);
        });
    },
    delete: (id, res)=>{
        Contato.findByIdAndDelete(id, (error, result)=>{
            if(error) res.json(error);
            res.json(result);
        });
    }
}

module.exports = functions;