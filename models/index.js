const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/adrianodb',{ useNewUrlParser: true });
var db = mongoose.connection;

db.on('error', console.error);
db.once('open', function () {
    console.log('Conectado ao MongoDB.')
});
const database = {};
database.mongoose = mongoose;
database.db = db;
module.exports = database;
